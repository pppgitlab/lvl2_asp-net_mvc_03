﻿using System.Web;
using System.Web.Mvc;

namespace LVL2_ASP.NET_MVC_03
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
