﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASP.NET_MVC_03.Models;

namespace LVL2_ASP.NET_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        Db_CustomerEntities DbModel = new Db_CustomerEntities();

        // GET: Customer
        public ActionResult Index()
        {
            return View(DbModel.Tbl_Customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {

            return View(DbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Tbl_Customer customer)
        {
            try
            {
                // TODO: Add insert logic here
                DbModel.Tbl_Customer.Add(customer);
                DbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(DbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Tbl_Customer customer)
        {
            try
            {
                // TODO: Add update logic here
                DbModel.Entry(customer).State = EntityState.Modified;
                DbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(DbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Tbl_Customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = DbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault();
                DbModel.Tbl_Customer.Remove(customer);
                DbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
